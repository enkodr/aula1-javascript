function ex1() {
    var num1 = document.getElementById("num1").value;
    var num2 = document.getElementById("num2").value;
    alert(parseInt(num1) + parseInt(num2));
}

function ex2() {
    document.getElementById("text2").value = document.getElementById("text1").value;
}

function ex3() {
    document.querySelector("#div-ex3").innerHTML = document.getElementById("txt-ex3").value;
}

function ex4() {
    for(var i=0; i<5; i++) {
        document.querySelector("#div-ex4").innerHTML += "<p>" + document.getElementById("txt-ex4").value + "</p>";
    }
}

function ex5() {
    document.getElementById("txt2-ex5").value += document.getElementById("txt1-ex5").value.split(" ");
    document.getElementById("txt1-ex5").value = "";
}

function ex6() {
    var words = document.getElementById("txt-ex6").value.split(" ");
    var ol = document.getElementById("ol-ex6");
    for(var i = 0; i < words.length; i++) {
        var li = document.createElement("li");
        li.innerHTML = words[i];
        ol.appendChild(li);
    }    
}

function ex7() {
    var textfield = document.querySelector("#text");
    var ul = document.querySelector("#list");
    var li = document.createElement("li");
    li.innerHTML = textfield.value;
    if(parseInt(textfield.value) % 2 == 0) {
        li.className = "even";
    } else {
        li.className = "odd";
    }
    ul.appendChild(li);
    textfield.value = "";
    textfield.focus();
}
